Simple app developed using MVP architectural pattern. It's a gallery of some of the main marvel heroes. 

I have used this Google's sample as a base for this project: https://github.com/googlesamples/android-architecture/tree/todo-mvp-dagger/